
public class practice02_1 {

	public static void main(String[] args) {

		int num = 20;

		if (num == 20) {
			System.out.println("numは20である。");
		}

		if (num != 10) {
			System.out.println("numは10ではない。");
		}

		if (num % 2 == 0) {
			System.out.println("numは偶数である。");
		}

		if (num % 3 == 2) {
			System.out.println("numを3で割った余りは2である。");
		}
	}
}
