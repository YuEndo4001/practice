
public class practice02_2 {

	public static void main(String[] args) {

		int year = 2100;

		if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0)) {

			System.out.printf("%d年はうるう年です。", year);

		} else {

			System.out.printf("%d年はうるう年ではありません。", year);

		}
	}
}
