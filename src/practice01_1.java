
public class practice01_1 {

	public static void main(String[] args) {

		int num = 3;
		int result = 0;

		// 加算
		result = 6 + num;
		System.out.println(6 + "+" + num + "=" + result);

		// 減算
		result = 6 - num;
		System.out.printf("6-%d=%d%n", num, result);

		// 乗算
		result = 6 * num;
		System.out.printf("6*%d=%d%n", num, result);

		// 除算
		result = 6 / num;
		System.out.printf("6/%d=%d%n", num, result);

		// 剰余
		result = 6 % num;
		System.out.println(6 + "%" + num + "=" + result);

	}
}
