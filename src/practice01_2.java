
public class practice01_2 {

	public static void main(String[] args) {

		int num = 3;
		int incrementResult = 0;
		int decrementResult = 0;

		// 1加算
		incrementResult = ++num;
		System.out.printf("3を1加算した数は%d%n", incrementResult);

		// 1減算
		num = 3;
		decrementResult = --num;
		System.out.printf("3を1減算した数は%d%n", decrementResult);

	}
}
